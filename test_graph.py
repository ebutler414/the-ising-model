#######################sample live updating graph##########################
#to plot a proof of concept graph that contains a slider and is live updating

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider



fig, ax = plt.subplots() # needs to be subplots to give seperate plots forthe slider and the graph, don't know what the ax does but necessary
plt.subplots_adjust(left=0.25, bottom=0.25)

'''
y_line = np.ones(100) # generates a list of ones, for the function y = 1
x_line = np.arange(0,100,1) #generates an array of x values continuious between 0 and 1000
graph, = plt.plot(x_line,y_line) # plots the function. both the graph and comma are essential, don't know why
'''
plt.axis([0,100,0,10]) # sets the axis correctly

ax1 = plt.axes([0.25, 0.1, 0.65, 0.03]) # creats the subfigure for the slider to be created
#ax2 = plt.axes()
slider1 = Slider(ax1, 'height', 0.1,10.0,valinit = 1.0) # definition of slider, arguments are (axes, name, min value, max value, valinit = initial value)

x_list = [0]
y_list = [0]
graph, = plt.scatter(x_list,y_list)

#defines the function that updates the graph when the slider is moved. must have variable (val) otherwise it does not work, don't know why
'''
def update(val):
    y_value = slider1.val #creates a variable new_height that is the new value of the slider
    graph.set_ydata(new_height*y_line) # sets the y data based on the function y = slider which contains the slider value
    fig.canvas.draw_idle() # don't know what this does but essential
'''
for i in range(5):
    y_value = slider1.val
    y_list.append(y_value)
    x_list.append(i)
    plt.pause(5.0)
    graph.set_ydata(y_list)


#slider1.on_changed(update) # calls the update function that changes the y values when it detects that the position of the slider has changed
'''
fig2 = plt.figure()
plt.ion()
plt.axis([0, 10, 0, 1])
for i in range(10):
    y = np.random.random()
    plt.scatter(i, y)
    plt.pause(0.05)
'''
while True:
    plt.pause(0.05)


plt.show()
