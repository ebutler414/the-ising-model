#!/usr/bin/env python

################################################################################
#########################  triangular_main.py   ################################
################################################################################

import triangular_lattice
import numpy as np
import pgmplot
import pbmplot

import matplotlib.pyplot as plt

lattice_1 = triangular_lattice.triangular_lattice(16,16)
big_matrix,matrix = lattice_1.genetic(64000,60)
plt.imshow(matrix)
np.savetxt('final_matrix.csv',matrix,delimiter = ",")


'''
lattice_1.update_pgmmatrix()
lattice_1.update_pbmmatrix()
lattice_1.energy()
matrix_1=lattice_1.matrix
matrix_2 = lattice_1.pbmmatrix
matrix_3 = lattice_1.pgmmatrix
pgmplot.plot(lattice_1.pgmmatrix,'triangular_4.pgm')
pbmplot.plot(lattice_1.pbmmatrix,'triangular_.pbm')


plt.imshow(lattice_1.matrix)
#plt.imshow(lattice_1.pbmmatrix)
plt.show()
print(lattice_1.matrix)
print(lattice_1.pgmmatrix)
'''
