#!/usr/bin/env python
# sample programming script to test the pbmplot.py function
import pbmplot 
import numpy

n=10
m=10
A=numpy.random.random_sample(n*m).reshape(n,m)
B=numpy.rint(A)

pbmplot.plot(B,"test.pbm")


