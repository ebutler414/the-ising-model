################################################################################
###########################   hysterisi.py   ###################################
################################################################################

import metropolis_algorithm
import numpy as np
import pbmplot

import matplotlib.pyplot as plt

lattice_1 = metropolis_algorithm.lattice(128,128) # creats an instance of the lattice class called lattice_1

number = 500 # number of intervals for the reading
lattice_1.KbT = 2.0
x_h = np.linspace(-0.5,0.5,number)#initialises the tempteratrue
lattice_1.h = -5.0
lattice_1.metropolis(10000000)
y_magnetisation = np.zeros(number)

count = 0
for i in x_h:
    lattice_1.h = i # sets KbT
    lattice_1.metropolis(100000) #allows the system to come to equilibrium (hopefully)
    lattice_1.magnetisation(15)

    y_magnetisation[count] = lattice_1.total_magnetisation
    count += 1
    print(count)

plt.plot(x_h,y_magnetisation)
#plt.imshow(lattice_1.matrix)
x_h_2 = np.linspace(0.5,-0.5,number)#initialises the tempteratrue
lattice_1.h = 5.0
lattice_1.metropolis(1000000)
y_magnetisation_2 = np.zeros(number)

count = 0
for i in x_h_2:
    lattice_1.h = i # sets KbT
    lattice_1.metropolis(100000)#allows the system to come to equilibrium (hopefully)
    lattice_1.magnetisation(15)

    y_magnetisation_2[count] = lattice_1.total_magnetisation
    count += 1
    print(count)#progress indicator

#plt.imshow(lattice_1.matrix)
#plt.plot(x_h_2,y_magnetisation_2)
#plt.show()
np.savetxt('x_h.csv',x_h, delimiter = ",")#dumps the arrays to csv files for plotting in origin
np.savetxt('y_magnetisation.csv',y_magnetisation, delimiter = ",")
np.savetxt('y_magnetisation_2.csv',y_magnetisation_2, delimiter = ",")
