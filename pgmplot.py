#!/usr/bin/env python
#pbm plotting module

#takes an n x m matrix A , rounds it and plots it as a .pbm file
#'file' is a string that contains the pbm file name and must contain the .pbm extension

import numpy as np
def plot(A,file):
    n,m = A.shape

    f=open(file,'w')
    f.write("P2 \n")
    f.write("#myplot module \n")
    st = str(n) + " " + str(m) + "\n"
    f.write(st)
    f.write("2\n")
    # rounds the matrix to one or zero
    #B = A.astype(bool)

    #saves the matrix to file
    np.savetxt(f, A)
