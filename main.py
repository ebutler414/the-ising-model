################################################################################
##########################   main.py   #########################################
################################################################################

import metropolis_algorithm
import numpy as np
import pbmplot
import cProfile
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider


lattice_1 = metropolis_algorithm.lattice(128,128) # creats an instance of the lattice class called lattice_1




'''
cProfile.run('lattice_1.metropolis(1000000)', sort = 'tottime')
lattice_1.update_pbmmatrix()
pbmplot.plot(lattice_1.pbmmatrix,'matrix_2.pbm ')


'''


'''
###########################multiple plots ######################################
# plots 5 graphs of lattice for different KbTs system should be random for large KbTs, and "clumped" for small KbT

KbT_range = [0.001, 1.0, 10, 10000, 100000000]#the values to be plotted, ew python lists
index  = 0 # index for formatting of output, used in order to generate graph_1 graph_2 etc.

for i in KbT_range:
    graph_lattice = metropolis_algorithm.lattice(128,128) #defines the instance
    graph_lattice.KbT = i #assigns KbT to the ith value of the list
    graph_lattice.metropolis(1000000) #allows the system to reach equilibrium (should be large final reading)
    graph_lattice.update_pbmmatrix() #after equilibrium has been reached generates a matrix that can be plotted
    pbmplot.plot(graph_lattice.pbmmatrix,"graph{0}.pbm".format(index)) #plots matrix.
    index += 1


lattice_1.KbT = 3 # sets KbT
lattice_1.metropolis(100000)
print(lattice_1.pbmmatrix)
print(lattice_1.matrix)
lattice_1.update_pbmmatrix()
print(lattice_1.pbmmatrix)
print(lattice_1.matrix)
lattice_1.energy()
energy = lattice_1.total_energy
matrix = lattice_1.matrix

lattice_1.magnetisation(10)
lattice_1.heat_capacity()
pbmmatrix = lattice_1.pbmmatrix
pbmplot.plot(pbmmatrix,'energy.pbm')
print("total energy of the lattice is:")
print(energy)
print("average magnitisation:")
print(lattice_1.total_magnetisation)
print("total heat capacity")
print(lattice_1.total_heat_capacity)
print("kbt")
print(lattice_1.KbT)

'''
number = 50 # number of intervals for the reading

x_KbT = np.linspace(5,1,number)#initialises the tempteratrue
y_energy = np.zeros(number) # initialises the emtpty arrays for plotting
y_heat_capacity = np.zeros(number)
y_magnetisation = np.zeros(number)
y_magnetic_suceptability = np.zeros(number)
count = 0
lattice_1.KbT = 5.0
lattice_1.metropolis(10000000)
for i in x_KbT:
    lattice_1.KbT = i # sets KbT
    lattice_1.metropolis(100000) #allows the system to come to equilibrium (hopefully)
    lattice_1.energy() #measures the energy, heat capacity, magnetisation, and magnetic suceptability
    lattice_1.heat_capacity()
    lattice_1.magnetisation()
    lattice_1.magnetic_suceptibility()

    y_energy[count] = lattice_1.total_energy #stores the y values
    y_heat_capacity[count] = lattice_1.total_heat_capacity
    y_magnetisation[count] = lattice_1.total_magnetisation
    y_magnetic_suceptability[count] = lattice_1.total_magnetic_suceptability
    lattice_1.update_pbmmatrix()
    pbmplot.plot(lattice_1.pbmmatrix,'gif_graph{0}.pbm'.format(count))
    count += 1
    print(count)

fig1 = plt.figure()#plots them
plt.plot(x_KbT,y_energy)
fig2 = plt.figure()
plt.plot(x_KbT,y_heat_capacity)
fig3 = plt.figure()
plt.plot(x_KbT,y_magnetisation)
fig4 = plt.figure()
plt.plot(x_KbT,y_magnetic_suceptability)
plt.show()
'''
np.savetxt('x_KbT.csv',x_KbT, delimiter = ",")#dumps the arrays to csv files for plotting in origin
np.savetxt('y_energy.csv',y_energy, delimiter = ",")
np.savetxt('y_heat_capacity.csv',y_heat_capacity, delimiter = ",")
np.savetxt('y_magnitisation.csv',y_magnetisation, delimiter = ",")
np.savetxt('y_magnetic_suceptability.csv',y_magnetic_suceptability, delimiter = ",")
'''
