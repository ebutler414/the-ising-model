#!/bin/bash
#a simple bash script that converts all of the pbm files in the folder to an animated gif
ls -v *.pbm>list.txt #lists all the pbm files in order cite https://stackoverflow.com/questions/1747047/bash-and-sort-files-in-order
convert -delay 5 -loop 0 @list.txt test.gif #delay is the time per frame
