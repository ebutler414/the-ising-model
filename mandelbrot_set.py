#!usr/bin/env python
import pbmplot
import numpy as np

n=1000
m=1000
A= np.random.random_sample(n*m).reshape(n,m)
for x in range(n):
    for y in range(m):
        
        p=4*(x/float(n)-0.5)
        q=4*(y/float(n)-0.5)
        
        c = p + 1j*q
        
        z = 0 + 0j

        for iter in range(100):
            if (abs(z) < 100):
                z = z ** 2 + c
                A[x,y]=0
            else:
                A[x,y]=1

pbmplot.plot(A,"test.pbm")
