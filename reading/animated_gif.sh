#!/bin/bash
#a simple bash script that converts all of the pbm files in the folder to an animated gif
array=`ls *.pbm|cat`#lists all the pbm files in order
convert -delay 30 -loop 0 $array test.gif #delay is the time per frame
