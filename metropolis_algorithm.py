#!/usr/bin/env python
##############################################################################
###########################Metropolis algorithem##############################
##############################################################################
import numpy
import random



def id(x): # cite http://ipython-books.github.io/featured-01/
    # This function returns the memory
    # block address of an array.
    return x.__array_interface__['data'][0]


def h(matrix,n,m,i,j,h = 0): # returns the hamiltonian of the point i,j for matrix. Period boundry implemented here

# since for the metropolis algorithm, if the code is functioning as intended, i -1 will never be out of bounds, the first two terms can be taken outside of the try statement
# note the order is important. Since for some reason for a lattice axis of length n the element [-1] = [n - 1]
# therefore the exception will only occur when you add +1 to the last element. this means that term 1 and term 2
# nevertheless be calculated meening it does not need to be handled in the exception and therefore can be brought outside try statement

    try:
        term_3 = matrix[i+1,j] #checks for an exception. this will only occur for i or j = n-1 since we are only considering nearest neighbours
        term_4 = matrix[i,j+1] #labelled term 3 and 4 for legacy reasons. Deleted terms 1 to 3 for speed

    except IndexError:
        #again order is important, the [n,m] case must be considered first as it satisfies the next two cases as well
        if i == n-1 and j == m-1:
            new_i = i - n
            new_j = j - m
            term_3 = matrix[new_i+1,new_j]
            term_4 = matrix[new_i,new_j+1]
        elif i == n - 1: #arising from zero based indexing
            new_i = i - n
            term_3 = matrix[new_i+1,j]
            term_4 = matrix[new_i,j+1]
        elif j == m - 1:
            new_j = j - m
            term_4 = matrix[i,new_j+1]

        else:
            print("something went wrong with periodic boundary")
            raise
    #defniniton of hamiltonian
    hamiltonian = - matrix[i,j]*(matrix[i-1,j]+matrix[i,j-1]+term_3+term_4) + h * matrix[i,j]

    return hamiltonian







class lattice:
    def __init__(self,n,m):
        self.matrix = numpy.random.random_sample(n*m).reshape(n,m) #creats a random matrix of elements between 0 and 1

        self.matrix = numpy.rint(self.matrix) # rounds to 0 and 1 creating a random matrix of 0s and 1s
        self.matrix = self.matrix.astype(int) # changes from float to int for speed
        self.pbmmatrix = numpy.zeros(n*m).reshape(n,m) # initialises a matrix full of 1s and 0s for the pbmfile.
        self.n = n #stores the dimensions of the matrix
        self.m = m
        self.total_energy = 0.0
        self.KbT = 1000.0 #boltzmann constant times Temp
        self.h = 0 # magnetic field
        self.total_magnetisation = 0.0
        self.total_heat_capacity = 0.1
        self.hamiltonian_matrix = numpy.zeros(n * m).reshape(n,m)# initialises the matrix with all the hamiltonina points


        for i in range(0,n): #seachres through thte matrix and changes 0 to -1
            for j in range(0,m):
                if self.matrix[i,j] == 0:
                    self.matrix[i,j] = -1




    def metropolis(self,number_of_iterations):#function that preforms the metropolis algorithm for a given number of iterations



        n = self.n #assigning another variable so that a local variable is being called rather than a global one
        m = self.m
        matrix = self.matrix
        KbT = self.KbT
        for i in range(0,number_of_iterations):

            #picks a random point
            #periodic boundary used, most of the implementation is done in the hamiltonian
            #n-1 is becasue of zero based indexing
            x = random.uniform(0,n-1)
            y = random.uniform(0,m-1)
            #rounds and converts into integer
            x = int(numpy.rint(x))
            y = int(numpy.rint(y))

            hamiltonian_old = h(matrix,n,m,x,y) # stores the old hamiltonian
            #flips the spin, and stores the result
            matrix[x,y] = -matrix[x,y]

            hamiltonian_new = h(matrix,n,m,x,y) # calculates the hamiltonian for the flipped spin

            delta_e =  hamiltonian_new - hamiltonian_old #calculates change in energy

            #if delta_e < 0 accept new configuration. Since the spin has already been flipped and stored no action is required

            if delta_e > 0:
                thermodynamic_probability = numpy.exp(- (float(delta_e) / (KbT))) # the probability that the system goes up in energy according to the boltzmann distribution
                random_number = random.random() # choses a random number between 1 and 0
                if thermodynamic_probability < random_number: # change back to origional spin if it looses the lottery, the hamiltonian increased in energy and the boltzmann factor was insuficient to kick it up the slope
                        matrix[x,y] = -matrix[x,y]

        self.matrix = matrix #note indent, is called when for loop finishes

    def sum_of_hamiltonians(self): # creats a matrix of all the hamiltoninas for each entry in self.matrix
        self.hamiltonian_matrix = numpy.zeros((self.n,self.m))
        for i in range(0,self.n):
            for j in range(0,self.m): #iterates over all the hamiltonians
                 self.hamiltonian_matrix[i,j] =  h(self.matrix,self.n,self.m,i,j)# assignes each element with its hamiltonian


    def energy(self,iterations = 10): #Average hamiltonian per lattice entry
        self.sum_of_hamiltonians()#updates the hamiltonian_matrix
        local_total_energy = 0 #initialises a local variable for total energy
        for i in range(0,iterations):#averages over a few sweeps
            self.metropolis(self.n*self.m) #sweeps once (should be m*n)
            self.sum_of_hamiltonians()#updates it after each iteration
            local_total_energy = numpy.sum(self.hamiltonian_matrix) / float(self.n * self.m)#sums all the hamiltonians, divides it by the number of points.
        local_total_energy = float(local_total_energy) / float(iterations)
        self.total_energy = local_total_energy#stores this in the lattice object
        print("total energy is:")
        print(self.total_energy)

    def update_pbmmatrix(self): #creats a matrix that can be plottable using a pbm file
        for i in range(0,self.n): #seachres through thte matrix and changes 0 to -1
            for j in range(0,self.m):
                if self.matrix[i,j] == -1:
                    self.pbmmatrix[i,j] = 0
                elif self.matrix[i,j] == 1:
                    self.pbmmatrix[i,j] = 1


    def magnetisation(self,iterations = 10):
        local_total_magnetisation = 0.0
	#magnitisation_matrix = numpy.zeros(self.n*self.m).reshape(self.n,self.m)

    	for z in range(0,iterations): #average magnitisation per point
            self.metropolis(self.n*self.m)

            average_magnetisation = numpy.sum(self.matrix) #sums the entiere magnitisation of the latttice
            average_magnetisation = float(average_magnetisation) / float(self.m * self.m)
            local_total_magnetisation += average_magnetisation

        self.total_magnetisation = float(local_total_magnetisation) / float(iterations) #assigns it to the object
        #heat capcity
    def heat_capacity(self,iterations = 10):
        self.sum_of_hamiltonians()
        local_total_heat_capcity = 0.0
        for i in range(0,iterations):#takes an average
            self.metropolis(self.n*self.m)#sweeps once through the system
            self.sum_of_hamiltonians()#updates the hamiltonian matrix
            hamiltonian_squared_matrix = numpy.square(self.hamiltonian_matrix)#squares each element term wise and assignes it to a new matrix

            average_of_the_squares = float(numpy.sum(hamiltonian_squared_matrix)) / float((self.n * self.m )) #average hamiltonian squared for each point

            average_squared = float(numpy.sum(self.hamiltonian_matrix)) / float((self.n * self.m)) #calculates the average hamiltonian per point
            average_squared = (average_squared)**2 # squares it
            local_total_heat_capcity += float(average_of_the_squares - average_squared) / float(self.KbT) #adds to the heat capcity

        local_total_heat_capacity = float(local_total_heat_capcity) / float(iterations)#the average
        self.total_heat_capacity = local_total_heat_capacity


    def magnetic_suceptibility(self,iterations = 10):
        local_magnetic_suceptability = 0
        for z in range(0,iterations):
            self.metropolis(self.n*self.m)#iterates one sweep

            local_total_magnetisation = float(numpy.sum(self.matrix)) / float((self.n * self.m))
            local_total_magnetisation = (local_total_magnetisation)**2

            local_magnetic_suceptability += float(1.0-local_total_magnetisation) / self.KbT
        self.total_magnetic_suceptability = local_magnetic_suceptability / float(iterations) #magnetic suceptability
