#!/usr/bin/env python
##############################################################################
########################Triangular lattice class##############################
##############################################################################
import numpy
import random



def id(x): # cite http://ipython-books.github.io/featured-01/
    # This function returns the memory
    # block address of an array.
    return x.__array_interface__['data'][0]

#triangular lattice
def h(matrix,n,m,i,j,k,h = 0): # returns the hamiltonian of the point i,j for matrix. Period boundry implemented here

#read comments in metropolis algorithm as well
#periodic boundary conditions implemented here
#also the triangular lattice is implemented here. It can be seen by the two extra cross terms
#had to add a constant i dimension since the matrix i was using had a dimension 3 and it was easier this way
#working on the jk plane

###########this should converge to -1 in all entrys since positive values of fitness are better and fitness \propto 1-H
    try:
        term_3 = matrix[i,j+1,k] #checks for an exception. this will only occur for i or k = n-1 since we are only considering nearest neighbours
        term_5 = matrix[i,j+1,k-1]
        term_4 = matrix[i,j,k+1] #labelled term 3 and 4 for legacy reasons. Deleted terms 1 to 3 for speed

    except IndexError:
        #again order is important, the [n,m] case must be considered first as it satisfies the next two cases as well
        if j == n-1 and k == m-1:
            new_j = j - n
            new_k = k - m
            term_3 = matrix[i,new_j+1,new_k]
            term_4 = matrix[i,new_j,new_k+1]
            term_5 = matrix[i,new_j+1,new_k-1]
        elif j == n - 1: #arising from zero based indexing
            new_j = j - n
            term_3 = matrix[i,new_j+1,k]
            term_4 = matrix[i,new_j,k+1]
            term_5 = matrix[i,new_j+1,k-1]
        elif k == m - 1:
            new_k = k - m
            term_4 = matrix[i,j,new_k+1]

        else:
            print("something went wrong with periodic boundary")
            raise
    #defniniton of hamiltonian
    hamiltonian = - matrix[i,j,k]*(matrix[i,j-1,k]+matrix[i,j,k-1]+term_3+term_4+matrix[i,j-1,k-1]+ term_5) + h * matrix[i,j,k] #note cross terms

    return hamiltonian







class triangular_lattice:
    def __init__(self,n,m):
        self.matrix = numpy.zeros((n,m)) #creats a placeholder matrix of zeros

        self.pbmmatrix = numpy.zeros(n*m).reshape(n,m) # initialises a matrix full of 1s and 0s for the pbmfile. Copy() is necessary as otherwise the matrixes would hav the same pointer
        self.n = n #stores the dimensions of the matrix
        self.m = m
        self.total_energy = 0.0
        self.h = 0 # magnetic field
        self.total_magnetisation = 0.0
        self.total_heat_capacity = 0.0
        self.hamiltonian_matrix = numpy.zeros(n * m).reshape(n,m)# initialises the matrix with all the hamiltonina points

        '''
        for i in range(0,n): #seachres through thte matrix and changes 0 to -1
            for j in range(0,m):
                if self.matrix[i,j] == 0:
                    self.matrix[i,j] = -1
'''




    def genetic(self,number_of_iterations,N):#function that preforms the metropolis algorithm for a given number of iterations
    #N needs to be even
    #N is the number of genetic strings


        n = self.n #assigning another variable so that a local variable is being called rather than a global one
        m = self.m
        genetic_code = numpy.random.random_sample(N * n *m).reshape(N,n*m) #creats a matrix of random 1s and 0s of all the genotypes, N genotypes of length L^2 (n*m)
        genetic_code = genetic_code * 2.0 -1.0 #converts to glass spin , RN between -1 and 1


        def mutation(self,genotype_number,i): #function will be used for calculating mutations, wish to define outside for loop
            #this function will only flip one spin point, can change more
            location = numpy.random.random() #location of the random flip
            location = int(numpy.rint(location * n*m-1))
            genotype_entry = genetic_code[genotype_number]
            genotype_entry[location] = numpy.random.random() * 2.0 - 1.0
            genetic_code_new[i] = genotype_entry

        for z in range(0,number_of_iterations):
            print(z)
            genetic_code_new = numpy.zeros(2*N*n*m).reshape(2*N,n*m)
            mating_list = random.sample(range(0,N),(N)) #creats a list of all the indexes for the genotype except in a random order, each pair is a mating partner
            indexing_array = numpy.arange(0,N,1)




            ###################################   mutations and recombinations ###########################
            #(probably could take this outside of the mutation function for speed but don't have time to rework it)

            for i in indexing_array[::2]: #selects even entrys, and treats them as pairs (two mating partners)
                #recombonation, probability = 50
                condition_1 = 0
                condition_2 = 0
                genotype_number_1 = mating_list[i] #the entry in genotype array that we are working in
                genotype_number_2 = mating_list[i+1]

                ################mutations

                if 0.3 > numpy.random.random():
                    mutation(self,genotype_number_1,i)
                    genetic_code_new[i+1] = genetic_code[genotype_number_2]
                    genetic_code_new[i+N] = genetic_code[genotype_number_1]
                    genetic_code_new[i+N+1] = genetic_code[genotype_number_2]
                    condition_2 = 1
                if 0.5 > numpy.random.random():
                    mutation(self,genotype_number_2,i+1)
                    if condition_2 == 0:
                        genetic_code_new[i] = genetic_code[genotype_number_1]
                    genetic_code_new[i+1] = genetic_code[genotype_number_2]
                    genetic_code_new[i+N] = genetic_code[genotype_number_1]
                    genetic_code_new[i+N+1] = genetic_code[genotype_number_2]
                    condition_2 == 1





                ######recombination
                #takes a section of code from one parent and swaps it in the same place with the other parent
                if 0.6 > numpy.random.random():#if a recombination is going to happen
                    condition_1 = 1
                    random_variable = numpy.random.random() #decides the interval for swapping
                    #in order to go from the interval 0 to 1 to x to y, multiply by y-x and add x
                    random_variable = random_variable * 3.0 + 3.0 #scales the random variable
                    exchange_size = int(numpy.rint(n*m/random_variable)) #turns it into a ratio


                    exchange_starting_point_1 = numpy.random.random() * (n*m - exchange_size)#figures out the starting point. Can't be further than n-exchange_number as that would be out of bounds. Scales this to that interval
                    exchange_starting_point_1 = numpy.rint(exchange_starting_point_1)#rounds this to an int to give a random number that will be the start of where the genotypes will be exchanged.
                    exchange_starting_point_1 = int(exchange_starting_point_1)


                    exchange_starting_point_2 = numpy.random.random() * (n*m - exchange_size)#figures out the starting point. Can't be further than n-exchange_number as that would be out of bounds. Scales this to that interval
                    exchange_starting_point_2 = numpy.rint(exchange_starting_point_2)#rounds this to an int to give a random number that will be the start of where the genotypes will be exchanged.
                    exchange_starting_point_2 = int(exchange_starting_point_2)
                    #we now have the length of the exchanged point, and the starting position. The first string will be exchanged with the second. This does not need to be random since the order of the parents is radnom to start with
                     #the second parent

                    #plucks the genetic code from the matrix with all the genotypes in it. This is for ease of use.
                    #for preformance this should really be removed
                    genotype_entry_1 = genetic_code[genotype_number_1] #the corrisponding genotype entry in the matrix
                    genotype_entry_2 = genetic_code[genotype_number_2]

                    #initialises the daughter genotype
                    genotype_entry_1_new = numpy.copy(genetic_code[genotype_number_1]) #daugher genotype
                    genotype_entry_2_new = numpy.copy(genetic_code[genotype_number_2])

                    #takes a snippit of length and size decided earlier and saves it as a local variable
                    genotype_snippit_1 = genotype_entry_1[exchange_starting_point_1:exchange_starting_point_1+exchange_size]
                    genotype_snippit_2 = genotype_entry_2[exchange_starting_point_2:exchange_starting_point_2+exchange_size]

                    #sets the corrisponding bit of code in the second array
                    #to be that snippet taken
                    genotype_entry_1_new[exchange_starting_point_1:exchange_starting_point_1+exchange_size] = genotype_snippit_2
                    genotype_entry_2_new[exchange_starting_point_2:exchange_starting_point_2+exchange_size] = genotype_snippit_1

                    genetic_code_new[i] = genotype_entry_1_new #fills the new matrix to be ranked by fitness
                    genetic_code_new[i+1] = genotype_entry_2_new
                    genetic_code_new[i+N] = genotype_entry_1
                    genetic_code_new[i+N+1] = genotype_entry_2

                #still need to copy the old and new genotypes if mutations or recombinations do not happen
                if condition_1 == 0 and condition_2 == 0:
                    genetic_code_new[i] = genetic_code[i] #fills the new matrix to be ranked by fitness
                    genetic_code_new[i+1] = genetic_code[i+1]
                    genetic_code_new[i+N] = genetic_code[i]
                    genetic_code_new[i+N+1] = genetic_code[i+1]

                '''
                if numpy.array_equal(genetic_code_new[i],numpy.zeros(m*m)) or numpy.array_equal(genetic_code_new[i+1],numpy.zeros(m*m)) or numpy.array_equal(genetic_code_new[i+N],numpy.zeros(m*m)) or numpy.array_equal(genetic_code_new[i+N+1],numpy.zeros(m*m)):
                    print('copying an array of zeros: assignment issue')
                '''
            #now need to convert from string (arrays of length n*m) back to a n,m matrix and calculate the hamiltonian for each matrix
            #formula for converting from string to matrix:
            # the nth element in the string is the i,j element of the matrix: n = (j-1)L + i
            #store them as a list of matrices (a quasi tensor)


            # a list of actual lattices
            actual_lattice_2N = numpy.zeros((2*N,n,m))
            #the same size lattice however now every point is the hamiltonian of the corrisponding points
            genetic_hamiltonian_matrix = numpy.zeros((2*N,n,m))

            #maps the code from a list of lattice entrys to actual lattice entrys, notice the dimension change
            #i know that the map is supposed to be i>=1 not i>=0 and that screws up the whole map but i don't care
            for i in range(0,2*N):
                for j in range(0,n):
                    for k in range(0,m):
                        actual_lattice_2N[i,j,k] = genetic_code_new[i,((k-1)*m + j)] #note it's j,k not i,j, that's cause i refers to N axis

            for i in range(0,2*N):
                for j in range(0,n):
                    for k in range(0,m):
                        #calculates the hamiltonians
                        genetic_hamiltonian_matrix[i,j,k] = h(actual_lattice_2N,n,m,i,j,k)#watch the indexes

            #first sums along the j axis, second sums along the k axis
            fitness_list = genetic_hamiltonian_matrix.sum(axis = 1) #returns a matrix summed along the i axis [not sure about axis = 1]
            fitness_list = fitness_list.sum(axis = 1) #see sketch for a better idea of why axis=1 again,

            fitness_list = 2 * n * m - fitness_list #application of fitness formula

            sorted_index_list = numpy.argsort(fitness_list) #returns an array of INDEXES in the order that would sort fitness_list
            #using these indexes it is possible to fetch the corrisponding entrys without having to rarrange the matrix


            count = 0
            #passes the top 50 percent back into the starting matrix and repeats the process
            #goes from N to 2N
            for i in sorted_index_list[N:2*N]:
                genetic_code[count] = genetic_code_new[i]
                count += 1


        final_matrix_index = sorted_index_list[-1]
        final_matrix = actual_lattice_2N[final_matrix_index,:,:]
        self.matrix = final_matrix
        return genetic_code_new,final_matrix

    def sum_of_hamiltonians(self): # creats a matrix of all the hamiltoninas for each entry in self.matrix
        self.hamiltonian_matrix = numpy.zeros(self.n * self.m).reshape(self.n,self.m)
        for i in range(0,self.n):
            for j in range(0,self.m): #iterates over all the hamiltonians
                 self.hamiltonian_matrix[i,j] =  h(self.matrix,self.n,self.m,i,j)# assignes each element with its hamiltonian


    def energy(self,iterations = 10): #Average hamiltonian per lattice entry
        self.sum_of_hamiltonians()#updates the hamiltonian_matrix
        local_total_energy = 0 #initialises a local variable for total energy
        for i in range(0,iterations):#averages over a few sweeps
            self.genetic(self.n*self.m) #sweeps once (should be m*n)
            self.sum_of_hamiltonians()#updates it after each iteration
            local_total_energy += numpy.sum(self.hamiltonian_matrix) / float(self.n * self.m)#sums all the hamiltonians, divides it by the number of points.
        local_total_energy = float(local_total_energy) / float(iterations)
        self.total_energy = local_total_energy#stores this in the lattice object
        print("total energy is:")
        print(self.total_energy)

    def update_pbmmatrix(self): #creats a matrix that can be plottable using a pbm file
        for i in range(0,self.n): #seachres through thte matrix and changes 0 to -1
            for j in range(0,self.m):
                if self.matrix[i,j] == -1:
                    self.pbmmatrix[i,j] = 0
                elif self.matrix[i,j] == 1:
                    self.pbmmatrix[i,j] = 1

    def update_pgmmatrix(self):
        self.pgmmatrix = numpy.zeros(self.n * self.m).reshape(self.n,self.m)
        for i in range(0,self.n): #seachres through thte matrix
            for j in range(0,self.m):
                self.pgmmatrix[i,j] = self.matrix[i,j] + 1.0 # want to do self.pgmmatrix = self.matrix + 2 but does not work.


    def magnetisation(self,iterations = 10):
        local_total_magnetisation = 0.0
	#magnitisation_matrix = numpy.zeros(self.n*self.m).reshape(self.n,self.m)

    	for z in range(0,iterations): #average magnitisation per point
            self.genetic(self.n*self.m)

            local_total_magnetisation += numpy.sum(self.matrix) #sums the entiere magnitisation of the latttice


        self.total_magnetisation = float(local_total_magnetisation) / float(self.n * self.m * iterations) #assigns it to the object
        #heat capcity
    def heat_capacity(self,iterations = 10):
        self.sum_of_hamiltonians()
        local_total_heat_capcity = 0.0
        for i in range(0,iterations):#takes an average
            self.genetic(self.n*self.m)#sweeps once through the system
            self.sum_of_hamiltonians()#updates the hamiltonian matrix
            hamiltonian_squared_matrix = numpy.square(self.hamiltonian_matrix)#squares each element term wise and assignes it to a new matrix

            average_of_the_squares = numpy.sum(hamiltonian_squared_matrix) / (self.n * self.m ) #average hamiltonian squared for each point

            average_squared = numpy.sum(self.hamiltonian_matrix) / (self.n * self.m) #calculates the average hamiltonian per point
            average_squared = (average_squared)**2 # squares it
            local_total_heat_capcity += float(average_of_the_squares - average_squared) / self.KbT #adds to the heat capcity

        local_total_heat_capacity = float(local_total_heat_capcity) / float(iterations)#the average
        self.total_heat_capacity = local_total_heat_capacity


    def magnetic_suceptibility(self,iterations = 10):
        local_magnetic_suceptability = 0
        for z in range(0,iterations):
            self.genetic(self.n*self.m)#iterates one sweep

            local_total_magnetisation = numpy.sum(self.matrix) / (self.n * self.m)
            local_total_magnetisation = (local_total_magnetisation)**2

            average_of_the_squares = (numpy.sum(self.matrix))**2 / (self.n * self.m )
            local_magnetic_suceptability += float(average_of_the_squares-local_total_magnetisation) / self.KbT
        self.total_magnetic_suceptability = local_magnetic_suceptability / float(iterations) #magnetic suceptability
