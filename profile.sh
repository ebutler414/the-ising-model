#!/bin/bash

filename='profile_output.txt'
if [ $# == 2 ]
then
file_name = $2
elif [[ $# != 1 || $# != 1 ]]
then
  echo "Usage: profile -script.py"
  echo "(optional parameters)"
  echo -"output_name.txt (default = profile_output.txt)"
  exit "125"
fi

python -m cProfile $1 -s tottime > $filename
