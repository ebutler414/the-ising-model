### The Ising Model
A Phase Transition in the square Lattice

![Phase Transition](https://media.giphy.com/media/xULW8Ay0rQr3xHx5tu/giphy.gif)

*All of this information is duplicated in the wiki tab*

This lovely source code was written to preform an investigation on the Ising model. There was two main parts to this investigation, the first was a standard investigation into basic properties of the square collinear ising model, such as hysterisis, magnetic suceptibility and heat capacity. Secondly an investigation was carried out to find the ground state of the triangula non-collinear spin glass lattice. This code is divided into three different sections. The first is the implementation of the metropolis algorithm. The second preforms the same function as the first however the main algorithm, along with all the functions that calculate the obserables are compiled into c code and imported as a module. The third is the genetic algorithm. A brief introduction to each is listed here then on the next pages the usage of them is discussed in full depth.

Firstly using the metropolis algorithm, the source code of which is contained within the `metropolis_algorithm.py`. This is run from the `main.py` script and contains the bog standard metropolis algorithm, calculating observables such as heat capacity, magnetic suceptability and then plotting them An annimated gif of the ising model will be uploaded.

The triangular spin glass was also examined. However instead of using the metropolis algorithm a genetic algorithm was used. A Further description will be uploaded. The genetic algorithm is contained within the '`triangular_lattice.py` script.

The third implementation is the standard metropolis algorithm, written in python, but wrapped in `cython` a module that takes python code and implements it in C. `cython` has numpy support built in and benifits from static type declaration. This can be found within the `cython` branch 

*note that the main branch is now redundant, this branch (`actual_spins_h_outside_class`) is the default branch. The other relevant branch is the `cython` branch.

***
#### Metropolis Algorithm

The metropolis algorithm is contained and most up to date within the `ࣴual_spins_h_outside_class` branch (default branch). The following scripts can be found within this branch

`animated_gif.sh`
A shell script that searches through the current directory, finds the files that end as `.pbm` expensions and sorts them in a list of most recently created (otherwise the order would be an issue), and converts them to an animated gif.

`hysterisis.py`
A python script that plots a hysterisis curve. It starts out with the magnetisation at 0.5T and records points decreasing to -0.5, then records the same graph the other way around and saves it to .txt files

`main.py`
the main python scritp that was used in the investigation, see chapter on it

`mandelbrot_set.py`
plots the mandelbrot set and saves it as a a `.pbm`

`metropolis_algorithm.py`
see chapter on it

`pbmplot.py`
takes a matrix of 1s and 0s, and plots it as a `.pbm`. The matrix must consist of 1s and 0s, Usage:
```python
import pbmplot as pbm
#call
pbmplot.plot(matrix,file)
```
file must be in the form `'some_file_name.pbm'`

`pgmplot.py`
attempted to make a pgm plotter for the non-collinear system, it did not work. Didn't know at the time that the matrices would need to be so small though so just used spyder variable explorer

`profile.sh`
A bugged script that profiles a python funciton, and dumps the results sorted by total time taken to a textfile. The program takes one manditory input, the python script to be profiled, and one optional input, the output textfile name, default = `profile_output.txt`. For some reason, the last command, `python -m cProfile $1 -s tottime > $filename` works in the CLI but not when run as a bash script, and it gives a python error, not a bash eror. Don't know why

`slider.py`
Opens a GUI with a slider to adjust $k_BT$. The script works by repeatedly saving the pbm file after each update, The live updating image is viewed by opening it in ImageViewer, contained either in KDE or unity desktop enviroments (i'm not sure which one the program comes with). THe default value for $k_BT$ is set to 1. Adjust default $k_BT$ by changing `KbT_init = 1` and chnage the range which the slider runs over by adjusting `sKbT = Slider(ax_KbT,'KbT,0.1,4, valinit = KbT_init)` where 0.1 is the lower limit and 4 is the upper limit

`triangular_lattice.py`
Genetic algorithm, see chapter

`triangular_main.py` where the investigation into the genetic algorithm was preformed, see wiki

***
#### Metropolis Algorithm

Welcome to the documentation on how to customise the metropolis algorithm code

The Metropolis algorithm is preformed in metropolis_algorithm.py script. The first function is `id`, this returns the location of a variable in memory, was used for optimising.

`h(matrix,n,m,i,j,h = 0)`
returns the hamiltonian of a coordinate `i,j` for a given `matrix`, pretty much nothing customisable here. As was mentioned in the report this could have been implemented using modulo operators, but I prefer my hashed together workaround solution.

```python
class lattice:
    def __init__(self,n,m):
```
This constructs a lattice consisting of an `n,m` matrix of random 1s and -1s, also where the default values are set. Change defualt values such as `KbT` or `h`etc. here. Usage:
```python
import metropolis_algorithm
lattice_1 = metropolis_algorithm(n,m)
```
creats a lattice full of -1s and 1s of size n,m This does not need to be square however for the investigation it was kept square.

`metropolis(self,number_of_iterations)`
Uses the metropolis algorithm to update the lattice. Nothing really customisable, usage:

```python
import metropolis_algorithm
lattice_1 = metropolis_algorithm(n,m)
lattice_1.metropolis(1000)
```
Where 1000 is the number of spin flips, **not sweeps**. One sweep = $m\times n$ spin flips, i.e. on average one update per lattice site.

`energy(self,iterations = 10)`
Assignes an attribute that is the average value of energy per lattice site in units of coupling constant. The function itself does not return anything. Since this is a random system in order to generate a mroe accurate reading, takes a multiple readings and sweeps throught the lattice once preforming `m*n` spin flips and averages it a number of times. Default number of averages is 10. Change using `iterations = number of averages`

Using the same syntax, functions can be called that update the magnetisation, heat capacity and magnetic sucepability. Function names are `magetisation`, `heat_capacity` and `magnetic_suceptibility` respectively

Example Usage:
```python
import metropolis_algorithm
lattice_1 = metropolis_algorithm(n,m)
lattice_1.energy(100)
my_lattice_energy = lattice_1.total_energy
```

All observables can be accessed using `self.total_[observable_name_here]`

`update_pbmmatrix(self)`
Assignes an attribute of the object to be a matrix consisting of 1s and 0s that represent the current state of the spin lattice. After this is called the attribute `self.pbmmatrix` can be called. *this function must be called before attempting to plot the lattice, if you do not call this before plotting the lattice, the default lattice, or an old lattice, will be plotted*

***
### Genetic Algorithm

Genetic algortithm contained within `triangular_lattice.py`. Usage is similar to metropolis algorithm, the class is constructed using the same syntax. Once a lattice has been initialised, the genetic algorithm is calle das follows

`genetic(self,number_of_iterations,N)`
Where `N`is the number of genotypes, and `number_of_iterations` is the number of times the fitness function is called. The function returns a tuple of a matrix wich contains all of the genotypes, and the final lattice mappped back into lattice format. (as a matrix)

Changable parameters, mutation and recombination probability. Edit the value of 

```python
if 0.3 > numpy.random.random():
```
for mutation and
 ```python
######recombination
#takes a section of code from one parent and swaps it in the same place with the other parent
if 0.6 > numpy.random.random():#if a recombination is going to happen
```
For recombination. 

Note that the functions below the genetic algorithm *are not implemented and will not return observables* do not use them. They have not been implimented yet.
