################################################################################
##########################   main.py   #########################################
################################################################################

import metropolis_algorithm
import pbmplot
import cProfile
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider


lattice_1 = metropolis_algorithm.lattice(128,128) # creats an instance of the lattice class called lattice_1

###################### attempted slider implementation #########################
#now working however it has a tendancy to crash plasma
#based on https://matplotlib.org/examples/widgets/slider_demo.html

KbT_init = 1 # initial KbT term
h_init = 0
fig,ax = plt.subplots() #for slider
plt.subplots_adjust(left=0.25, bottom=0.25) # for slider, don't fully know what it does


ax_KbT = plt.axes([0.25, 0.1, 0.65, 0.03]) #don't fully know what it does but necessary
ax_h = plt.axes([0.5, 0.2, 0.8, 0.06]) #don't fully know what it does but necessary

sKbT = Slider(ax_KbT,'KbT',0.1,4, valinit=KbT_init) # definition of slider object for KbT
sh = Slider(ax_h,'h',0.1,4, valinit=h_init) # definition of slider object for KbT

def update(val): #updates the lattice KbT to the value of the slider. Not currently implemented
    lattice_1.KbT = sKbT.val
sKbT.on_changed(update)


while True: #indefininite while loop
    lattice_1.KbT = sKbT.val #sets the lattice kbt to the slider value
    lattice_1.h = sh.val
    lattice_1.metropolis(100000) #allows the lattice to evolve in time by applying the metropolis algorithm
    lattice_1.update_pbmmatrix() # must be called in order to update pbmmatrix (the matrixo consisting of 1s and 0s for pbm file)
    pbmplot.plot(lattice_1.pbmmatrix,'matrix_2.pbm ') #plots the pbmmatrix
    plt.pause(0.05) #necessary, facilitates the showing of the slider and pauses the system for some time. experiment with this value for optimisation

plt.show()
